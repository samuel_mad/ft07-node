/**
 * recibir datos por POST en nuestra app.
 * 1. Lo primero sera identificar la url y el method donde recogeremos los datos
 * 2. una vez filtrado por metodo y url creamos un array al que llamaremos body .
 * 3. Usaremos el metodo on() del request, el cual recibe el tipo de evento, en
 *    este caso 'data' y un callback cuyo parametro sera los trozos de codigo chunk
 * 4. vamos almacenando esos chunks en nuestro array body cn el metodo push del mismo
 * 5. para saber cuando finaliza disponemos del evento end, que escurraremos igual que 
 *    en el caso anterior con el metodo 'on' del request.
 * 6. En el callback de on parsearemos los datos para poder usarlos, para ello usaremos
 *    el objeto Buffer, el cual dispone de un metodo concat, el cual admite un array
 *    como parametro 
 */
