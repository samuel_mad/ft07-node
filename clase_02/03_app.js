/**
 * Vamos a crear una pagina y servirla mediante node, la pagina va a contener un formulario con un input
 * donde vamos a recoger un dato para enviarlo al back.
 * 1. Primero generamos la carpeta views
 * 2. Generamos un HTML (form.html
 * 3. En nuestro index, app o el nombre que le hayamos asignado, vamos a requerir el modulo fs (File System)
 * 4. Usaremos el metodo readFile
 * 5. Si inspeccionamos el metodo en la documentancion de node, recibe 2 parametros, la ruta y un callback
 * 6. El callback nos da datos y lo pasamos en la respuesta en su metodo write.  
 */
