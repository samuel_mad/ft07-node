/**
 * Cuando ejecutamos un script con node, podemos necesitar configurarlo en el propio
 * lanzamiento de la ejecución. Esto lo hacemos mediante variables que almacenamos en
 * el proceso que se esta lanzando y accedemos a ellas desde nuestro código.
 */



/**
 * Ejercicio: ¿Recuerdas las funciones matemáticas que desarrollamos antes?, pues...
 * Me parece que sabes lo que toca.
 *  1. Importamos el modulo que ya habiamos creado 
 *  2. En lugar de setear directamente las variables, las recogeremos del proceso
 *  3. Corre el script pasando los valores de a y b 
 */