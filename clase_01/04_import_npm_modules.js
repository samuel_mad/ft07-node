/**
 * Importaremos un modulo con npm (Node Package Manager) y veremos como
 * meterlo en nuestro codigo y usarlo. Para este ejemplo vamos a utilizar
 * la bibliotecavalidator.
 */

/**
 * Ejercicio: Haremos lo mismo que hemos hecho pero con la biblioteca chalk
 *  1. Acceder a la web de npm para buscar el recurso.
 *  2. Lo añadimos a nuestro proyecto como dependencia
 *  3. Siguiendo las indicaciones de la documentacion, pintamos algunos mensajes
 *     por consola, con colorinchis.
 */