/**
 * Creamos e importamos un modulo propio dentro de este, para ello
 * hacemos uso del metodo require() que nos provee la API de Node
 */






/**
 * Ejercicio: Creamos un modulo operaciones.js que exportara funciones
 * matemáticas.
 *  1. Generar el archivo operaciones.js
 *  2. Importarlo aqui mediante require en una constante mathFns
 *  3. En operaciones.js crearemos las funciones:
 *      - Suma
 *      - Resta
 *      - Multiplicacion
 *      - Division
 *     Todas las funciones recibiran 2 parametros (a, b)
 *  4. Exportaremos todas las funciones.
 *  5. Invocamos las funciones en nuestro script principal
 *  6. Corremos el archivo con Node.   
 */