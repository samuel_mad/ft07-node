/**
 * Cuando queramos usar varios argumentos, y poder especificar una clave para acceder a ellos
 * o logicas similares, tendremos que escribir nuestra logica, para poder filtrar los argumentos
 * que nos estan pasando, o podemos utilizar alguno ya escrito. Uno muy usado por la comunidad
 * es yargs.
 */


 /**
  * Pasar argumentos y recuperarlos por la clave indicada
  * node.js ejemplo.js --argumentoA=55 --argumentoB=10
  * node.js ejemplo.js --argumentoA 55 --argumentoB 10
  */

 /**
 * Utilizar los argumentos como Booleanos
 */

 /**
  * Recibir y almacenar parametros extra en un array que podemos usar o no
  */





/**
 * Ejercicio: ¿Recuerdas las funciones matemáticas que desarrollamos antes?, pues...
 * Me parece que sabes lo que toca. Un refactor como la copa de un pino
 *  1. Importamos el modulo que ya habiamos creado 
 *  2. En lugar de setear directamente las variables, las recogeremos del proceso yargs
 *  3. Corre el script pasando los valores de a y b 
 */